var adverbDict = {
  "あまり": "余り",
  "いたって": "至って",
  "おおいに": "大いに",
  "おそらく": "恐らく",
  "がいして": "概して",
  "かならず": "必ず",
  "かならずしも": "必ずしも",
  "かろうじて": "辛うじて",
  "きわめて": "極めて",
  "ことに": "殊に",
  "さらに": "更に",
  "じつに": "実に",
  "すくなくとも": "少なくとも",
  "すこし": "少し",
  "すでに": "既に",
  "すべて": "全て",
  "せつに": "切に",
  "たいして": "大して",
  "たえず": "絶えず",
  "たがいに": "互いに",
  "ただちに": "直ちに",
  "たとえば": "例えば",
  "ついで": "次いで",
  "つとめて": "努めて",
  "つねに": "常に",
  "とくに": "特に",
  "とつぜん": "突然",
  "はじめて": "初めて",
  "はたして": "果たして",
  "はなはだ": "甚だ",
  "ふたたび": "再び",
  "まったく": "全く",
  "むろん": "無論",
  "もっとも": "最も",
  "もっぱら": "専ら",
  "わずか": "僅か",
  "わりに": "割に",
}

function isAdverb(token) {
  if (adverbDict[token.surface] !== undefined &&
    token.tags[0] == '副詞' &&
    token.tags[1] == '一般') {
    return true;
  } else {
    return false;
  }
}

function validateSentence(sentence) {
     // sentence <- 文 ;
     for(var i = 0; i < sentence.tokens.length; i++){
       if(isAdverb(sentence.tokens[i])){
         addError("次の単語を利用すべきです。現状 : [" + sentence.tokens[i].surface +"] , 推奨 : [" + adverbDict[sentence.tokens[i].surface] +"]", sentence);
       }
     }
}

// optionally, you can implement validation logic for document, section
/*
 function preValidateSentence(sentence) {
 }
 function preValidateSection(section) {
 }
 function validateDocument(document) {
 }
 function validateSection(section) {
 }
*/
